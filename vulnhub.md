Vulnhub Boxes
=============

Silky-CTF: 0x02
---------------

- Command injection on http://192.168.0.1/admin.php?username=/usr/bin/id
- User flag: /home/silky/flag.txt
- Executable file /home/silky/cat_shadow vulnerable to buffer overflow
- Running `./cat_shadow $(perl -e 'print "bYlI" x 100')` will display shadow file
- Cracking hashes to get root password 'greygrey'
- Root flag: /root/flag.txt